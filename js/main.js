const localArray = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9],
];


class Sudoka {
    constructor(unresolvedArray) {
        this.unresolvedArray = unresolvedArray;
    }

    checkRepeatValuesRow(array){
        let mistake = 0
        for (let subArray = 0; subArray < array.length; subArray++) {
             for (let value = 0; value < array[subArray].length; value++) {
                  for (let checked = 0; checked < array[subArray].length; checked++) {
                       if(subArray == subArray && value == checked){
                            continue
                       }
   
                       if(array[subArray][value] == 0 && array[subArray][checked]==0){
                            continue
                       }
   
                       if(array[subArray][value] == array[subArray][checked]){
                            mistake++
                            
                       }
                  }
             }
             
        }
        return mistake > 0 ? false : true
   }

   checkRepeatValuesCol(array){
    let mistake = 0

    for (let subArray = 0; subArray < array.length; subArray++) {
         for (let value = 0; value < array[subArray].length; value++) {
              for (let checked = 0; checked < array[subArray].length; checked++) {

                   if(subArray == subArray && subArray == checked){
                        continue
                   }

                   if(array[subArray][value] == 0 && array[checked][value]==0){
                        continue
                   }

                   if(array[subArray][value] == array[checked][value]){
                        mistake++    
                   }
              }
         }
         
    }

    return mistake > 0 ? false : true
    
}
    changeZero(array){
        let arrayZero =  array.map((el, elIndex) => {
            return [[elIndex],[el.indexOf(0)]]
        }) 
        return arrayZero.filter(e => e[1] >= 0)
    }

    fillArray(array){
        let localArray = [...array]
        let zeroArray = this.changeZero(localArray)
             for (let index2 = 1; index2 <= 9; index2++) {
                  localArray[zeroArray[0][0]][zeroArray[0][1]] = index2;
   
                  if (this.checkRepeatValuesRow(localArray) && this.checkRepeatValuesCol(localArray)) {
                       return localArray
                  } else {
                       continue;
                  }
             }
   }


    sudoka(array){
        let newArray = [...array];
        
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        this.fillArray(newArray)
        
        return newArray
   }

   

}


let sudoka = new Sudoka(localArray[0])
console.log(sudoka.sudoka(localArray))




// function sudokuSolution(array){
//      // Проверка на количество переданных в массиве элементов
//      // if(array.length != 9){
//      //      alert('Количество массивов не соответствует 9')
//      // }

//      function checkRepeatValuesRow(array) {
//          let checkRepeatValuesRow = array.map((element) => {

//              const clearZeroInArray = element.filter((el) => el > 0);
             
//              const findRepeatValue = clearZeroInArray.filter(
//                  (el, index, arrayEl) => arrayEl.indexOf(el) !== index
//              );
//                return findRepeatValue.length
             
//          });
//          let delZero = checkRepeatValuesRow.filter(e=> e > 0).length
//          console.log(delZero)
//          if (delZero >= 0) return false
//      } 
     
//      if(checkRepeatValuesRow(array)){
//           alert('Изначальные данные некорректны')
//      }

//      function 

//      console.log(checkRepeatValuesCol(array))
     
// }



// console.log(sudokuSolution(localArray))


// let localArray = [...array]

     // function findZero(array){
     //      let localArray = []
     //      array.forEach(element => {
     //      localArray.push(element.indexOf(0)) 
     //      });
     //      return localArray
     // }

     // function checkTemplate(array){
     
     // }
     // console.log(findZero(array))
     // // Берём число массива по индексу индекс массива с 0

     // return localArray

// }
     
// for (let index = 0; index < localArray.length; index++) {
//      for (let subArray = 0; subArray < localArray[index].length; subArray++) {
//           let indexZero = localArray[subArray].indexOf(0);
//           for (let podbor = 1; podbor <= 9; podbor++) {
//               let duplicat = [...localArray];
//               duplicat[subArray][indexZero] = podbor;

//               if(!this.checkRepeatValuesRow(duplicat)){
//                break
//               }
//               if(!this.checkRepeatValuesCol(duplicat)){
//                break
//               }
//               localArray[subArray][indexZero] = podbor;
//           //     console.log(localArray)

//           //     if (this.checkRepeatValuesCol(duplicat) && this.checkRepeatValuesRow(duplicat)) {
//                //    localArray[subArray][indexZero] = podbor;
//                //    break;
              
//           }
//       }
     



// Верное решение, но только 1 раз
     // for (let subArray = 0; subArray < localArray.length; subArray++) {
     //      let indexZero = localArray[subArray].indexOf(0)
     //      for (let podbor = 1; podbor <= 9; podbor++) {
     //           let duplicat = [...localArray]
     //           duplicat[subArray][indexZero] = podbor
     //           if(this.checkRepeatValuesCol(duplicat) && this.checkRepeatValuesRow(duplicat)){
     //                localArray[subArray][indexZero] = podbor
     //                break
     //           }
     //      }
          
     // }




